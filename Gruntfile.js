module.exports = function(grunt){
  grunt.initConfig({
    //html minify
    htmlmin: {                                     // Task
      www: {                                      // Target
        options: {                                 // Target options
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'www/index.html' : 'build/index.html'
        }
      },
    },
    //copy css related files and save in build folder
    copy:{
      buildFiles:{
        expand: true,
        src: ['css/images/**','css/images/**/*'],
        cwd: 'src',
        dest: 'build/'
      },
      wwwFiles:{
        expand: true,
        src: ['css/images/**','css/images/**/*'],
        cwd: 'src',
        dest: 'www/'
      }
    },
    //concet
    concat:{
      // concat the JS files and save in build folder
      js:{
        src: [
          'src/js/jquery.js',
          'src/js/jquery-migrate.js',
          'src/js/jquery.mobile.js',
          'src/js/script.js'],
          dest: 'build/js/merged.js'
        },
        // concat the CSS files and save in build folder
        css:{
          src: [
            'src/css/jquery.mobile.css',
            'src/css/tsJmobileTheme1.css',
            'src/css/jquery.mobile.icons.min.css',
            'src/css/style.css'
          ],
          dest: 'build/css/merged.css'
        },
        // concat the HTML files and save in build folder
        html:{
          src: [
            'src/inc/header.html',
            'src/inc/page1.html',
            'src/inc/page2.html',
            'src/inc/page3.html',
            'src/inc/footer.html'
          ],
          dest: 'build/index.html'
        }
      },
      //uglify and save in www folder
      uglify: {
        js: {
          files: {
            'www/js/merged.js': ['build/js/merged.js']
          }
        }
      },
      // css minify and dave in www folder
      cssmin: {
        target: {
          files: {
            'www/css/merged.css': ['build/css/merged.css']
          }
        }
      },
      // keep a watch and trigger task when changes detected
      watch:{
        default:{
          // if any change in css or js or html file
          files:['src/js/**/*.js', 'src/css/**/*.css', 'src/inc/*.html'],
          // trigger below tasks
          tasks: ['concat','uglify','cssmin', 'copy','htmlmin']
        }
      }
    });

    // dependencies / PLUGINS
    // install command
    // npm install PLUGIN-NAME-HERE --save-dev

    // npm install grunt-contrib-copy --save-dev
    grunt.loadNpmTasks('grunt-contrib-copy'); // copy files

    // npm install grunt-contrib-concat --save-dev
    grunt.loadNpmTasks('grunt-contrib-concat'); // concat files

    // npm install grunt-contrib-uglify --save-dev
    grunt.loadNpmTasks('grunt-contrib-uglify'); //uglify and minify

    // npm install grunt-contrib-cssmin --save-dev
    grunt.loadNpmTasks('grunt-contrib-cssmin'); // minify

    // npm install grunt-contrib-watch --save-dev
    grunt.loadNpmTasks('grunt-contrib-watch'); // watch for changes.

    // npm install grunt-contrib-htmlmin --save-dev
    grunt.loadNpmTasks('grunt-contrib-htmlmin'); // html minification

  };
