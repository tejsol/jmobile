Install:
a. open terminal window (in windows, cmd )
b. navigate to the project folder (in windows for me -> cd c:\xamp2\htdocs\grunt\jmobile1)
1. (if you dont have grunt) npm install grunt --save-dev
2. install required plugins
    npm install grunt-contrib-copy --save-dev
    npm install grunt-contrib-concat --save-dev
    npm install grunt-contrib-uglify --save-dev
    npm install grunt-contrib-cssmin --save-dev
    npm install grunt-contrib-watch --save-dev
    npm install grunt-contrib-htmlmin --save-dev

-----------------------------
HOW TO RUN:
a. open terminal window (in windows, cmd )
b. navigate to the project folder (in windows for me -> cd c:\xamp2\htdocs\grunt\jmobile1)
c. run command 'grunt watch'
// grunt will now be watching over the folder and if any file changes in /src/ folder, will trigger the tasks like minify,uglify,copy,etc.

-----------------------------

SCRAP PAD / Notes:

Gruntfile.js
this file has all required steps for grunt.
when an js/css/html file is saved. various tasks like minify,concat,copy etc are triggered. this are all configured in this file.


the folder/file structure:
/src/ - source folder - contains all html/css/js/etc files in it. this is our source.

grunt creates /build/ and /www/ folders and files.
/www/ is the distribution folder or public view folder, or whatever you would like to call it.

--------------------------------------
/www/ folder has the minified index.html file. 
1. This /build/index.html file is concat of 
/src/inc/header.html
/src/inc/page1.html
/src/inc/page2.html
/src/inc/page3.html
/src/inc/footer.html
2. the /www/index.html is minified of /build/index.html

--------------------------------------
/www/js/merged.js  is a concat and uglified file.
1. concat of following files is saved as /build/js/merged.js
  'src/js/jquery.js',
  'src/js/jquery-migrate.js',
  'src/js/jquery.mobile.js',
  'src/js/script.js' // use this for custom javascript
2. uglified file from /build/js/merged.js is saved as /www/js/merged.js

--------------------------------------
/www/css/merged.css  is a concat and minified
1. concat of following files is saved as /build/css/merged.css
	'src/css/jquery.mobile.css',
	'src/css/tsJmobileTheme1.css',
	'src/css/jquery.mobile.icons.min.css',
	'src/css/style.css' // use this for custom styling
2. minified file from /build/css/merged.css is saved as /www/css/merged.css
3. 'src/css/tsJmobileTheme1.css' is a custom them made on https://themeroller.jquerymobile.com/

------------------------------
all files in /src/css/images/*.* are copied to 
/build/css/images/*.* and 
/www/css/images/*.*

----